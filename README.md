Launching project with docker: 

1) Pull project and go to it's main folder.
2) In application folder, copy env.example to env and adjust if necessary.
3) Go back to main folder and run "docker-compose up --build"
4) Enter php container "docker-compose exec php sh"
5) Run "cd /var/www/html"
6) Run "composer install"
7) php artisan key:generate
8) If you get permission error from laravel logs, go to application folder and run "sudo chmod o+w ./storage/ -R"
9) From main folder enter mysql container "docker-compose exec mysql sh"
10) Run "mysql -uroot -proot"
11) Run "create database laravel;"
12) Return to php container and run migrations (php artisan migrate)
13) Seed table with test data from php container (php artisan db:seed) 

Now you can check if app works on url "http://localhost:8080/"

Api for each model can be accessed using attached postman library