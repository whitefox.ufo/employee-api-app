<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'fullName' => $this->fullName,
            'dateOfBirth' => $this->dateOfBirth,
            'type' => $this->type,
            'pay' => $this->pay,
            'monthlyPay' => $this->monthly_pay,
            'position' => $this->position,
            'department_id' => $this->department_id,
            'department' => new DepartmentResource($this->department),
        ];
    }
}
