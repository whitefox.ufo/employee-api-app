<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'fullName' => ['required', 'string'],
            'dateOfBirth' => ['nullable', 'date'],
            'type' => ['nullable', 'in:salary,hourly'],
            'pay' => ['nullable', 'numeric'],
            'position' => ['nullable', 'string'],
            'department_id' => ['required', 'exists:departments,id'],
            ];
    }
}
