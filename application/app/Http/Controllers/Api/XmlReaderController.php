<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsertFileRequest;
use App\Models\Department;
use App\Models\Employe;
use function Symfony\Component\Console\Style\success;

class XmlReaderController extends Controller
{
    /**
     * @param InsertFileRequest $request
     */
    public function readXml(InsertFileRequest $request)
    {
        $insertType = $request->insertType;
        $file = $request->file->getPathName();

        $xmlDataString = file_get_contents($file);
        $xmlObject = simplexml_load_string($xmlDataString);

        $json = json_encode($xmlObject);
        $phpDataArray = json_decode($json, true);

        if ($insertType === 'department') {
            $this->insertDepartment($phpDataArray);
        } else if ($insertType === 'employee') {
            $this->insertEmploye($phpDataArray);
        }
    }

    private function insertDepartment(array $phpDataArray)
    {
        if (count($phpDataArray['department']) > 0) {

            $dataArray = array();

            foreach ($phpDataArray['department'] as $index => $data) {

                $dataArray[] = [
                    "name" => $data['name'],
                ];
            }

            Department::insert($dataArray);
        }

        return response('Departments imported', 200)->header('Content-Type', 'text/plain');
    }

    private function insertEmploye(array $phpDataArray)
    {
        if (count($phpDataArray['employee']) > 0) {

            $dataArray = array();

            foreach ($phpDataArray['employee'] as $index => $data) {

                $dataArray[] = [
                    "fullName" => $data['fullName'],
                    "dateOfBirth" => $data['dateOfBirth'],
                    "type" => $data['type'],
                    "pay" => $data['pay'],
                    "position" => $data['position'],
                    "department_id" => $data['department_id'],
                ];
            }

            Employe::insert($dataArray);
        }

        return response('Employees imported', 200)->header('Content-Type', 'text/plain');
    }
}
