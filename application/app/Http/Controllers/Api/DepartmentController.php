<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\DepartmentServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDepartmentRequest;
use App\Http\Resources\DepartmentResource;
use App\Models\Department;

class DepartmentController extends Controller
{
    /**
     * @var \App\Contracts\Services\DepartmentServiceContract
     */
    private DepartmentServiceContract $departmentService;

    /**
     * @param \App\Contracts\Services\DepartmentServiceContract $departmentService
     */
    public function __construct(DepartmentServiceContract $departmentService)
    {
        $this->departmentService = $departmentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $departments = $this->departmentService->get();

        return DepartmentResource::collection($departments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDepartmentRequest $request
     *
     * @return DepartmentResource
     */
    public function store(StoreDepartmentRequest $request)
    {
        $department = $this->departmentService->create($request->validated());

        return $this->show($department);
    }

    /**
     * Display the specified resource.
     *
     * @param Department $department
     *
     * @return DepartmentResource
     */
    public function show(Department $department)
    {
        return new DepartmentResource($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Department $department
     * @param StoreDepartmentRequest $request
     *
     * @return DepartmentResource
     */
    public function update(Department $department, StoreDepartmentRequest $request)
    {
        $department = $this->departmentService->update($department, $request->validated());

        return $this->show($department);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Department $department
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $this->departmentService->delete($department);

        return response()->noContent();
    }
}
