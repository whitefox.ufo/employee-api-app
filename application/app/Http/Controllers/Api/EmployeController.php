<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\EmployeServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeRequest;
use App\Http\Resources\EmployeResource;
use App\Models\Employe;
use Illuminate\Http\Request;

class EmployeController extends Controller
{
    /**
     * @var \App\Contracts\Services\EmployeServiceContract
     */
    private EmployeServiceContract $employeService;

    /**
     * @param \App\Contracts\Services\EmployeServiceContract $employeService
     */
    public function __construct(EmployeServiceContract $employeService)
    {
        $this->employeService = $employeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $employes = $this->employeService->get($request);

        return EmployeResource::collection($employes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEmployeRequest $request
     *
     * @return EmployeResource
     */
    public function store(StoreEmployeRequest $request)
    {
        $employe = $this->employeService->create($request->validated());

        return $this->show($employe);
    }

    /**
     * Display the specified resource.
     *
     * @param Employe $employe
     *
     * @return EmployeResource
     */
    public function show(Employe $employe)
    {
        return new EmployeResource($employe);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Employe $employe
     * @param StoreEmployeRequest $request
     *
     * @return EmployeResource
     */
    public function update(Employe $employe, StoreEmployeRequest $request)
    {
        $employe = $this->employeService->update($employe, $request->validated());

        return $this->show($employe);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employe $employe
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employe $employe)
    {
        $this->employeService->delete($employe);

        return response()->noContent();
    }
}
