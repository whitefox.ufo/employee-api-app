<?php

namespace App\Contracts\Services;

use App\Models\Employe;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface EmployeServiceContract
{
    /**
     *
     * @param $request
     *
     * @return LengthAwarePaginator
     */
    public function get($request): LengthAwarePaginator;

    /**
     * @param array $data
     *
     * @return Employe
     */
    public function create(array $data): Employe;

    /**
     * @param Employe $employe
     * @param array $data
     *
     * @return Employe
     */
    public function update(Employe $employe, array $data): Employe;

    /**
     * @param Employe $employe
     * @return void
     */
    public function delete(Employe $employe): void;
}