<?php

namespace App\Contracts\Services;

use App\Models\Department;
use Illuminate\Database\Eloquent\Collection;

interface DepartmentServiceContract
{
    /**
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get(): Collection;

    /**
     * @param array $data
     *
     * @return Department
     */
    public function create(array $data): Department;

    /**
     * @param Department $department
     * @param array $data
     *
     * @return Department
     */
    public function update(Department $department, array $data): Department;

    /**
     * @param Department $department
     *
     * @return void
     */
    public function delete(Department $department): void;
}