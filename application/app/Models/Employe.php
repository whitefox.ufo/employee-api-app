<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Employe
 *
 * @property int $id
 * @property int $department_id
 * @property int $type
 * @property float $pay
 * @property float-read $monthly_pay
 * @property string $fullName
 * @property string $dateOfBirth
 * @property string $position
 */
class Employe extends Model
{
    use HasFactory;

    protected $fillable = [
        'fullName',
        'dateOfBirth',
        'type',
        'pay',
        'position',
        'department_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

    /**
     * @return float
     */
    public function getMonthlyPayAttribute(): string
    {
        if ($this->type === 'salary') {
            $monthlyPay = $this->pay;
        } else {
            $monthlyPay = $this->pay * 7 * 22;
        }

        return $monthlyPay;
    }
}
