<?php

namespace App\Services;

use App\Contracts\Services\EmployeServiceContract;
use App\Models\Employe;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class EmployeService implements EmployeServiceContract
{
    /**
     * {@inheritDoc}
     */
    public function get($request): LengthAwarePaginator
    {
        $itemPerPage = $this->getPaginationLimit($request->query('per_page'));

        $query = Employe::with('department');
        $query = $this->filterEmployees($query, $request);

        return $query->paginate($itemPerPage);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Employe
    {
        $employe = Employe::create($data);

        return $employe;
    }

    /**
     * {@inheritDoc}
     */
    public function update(Employe $employe, array $data): Employe
    {
        $employe->update($data);

        return $employe;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(Employe $employe): void
    {
        $employe->delete();
    }

    /**
     * @param int|null $per_page
     *
     * @return int|int|null
     */
    private function getPaginationLimit(?int $per_page)
    {
        return $per_page ?? $per_page | 10;
    }

    /**
     * @param Builder $query
     * @param Request $request
     * @return Builder
     */
    private function filterEmployees(Builder $query, Request $request)
    {
        if ($department = $request->query('department')) {
            $query->where('department_id', $department);
        }

        return $query;
    }
}
