<?php

namespace App\Services;

use App\Contracts\Services\DepartmentServiceContract;
use App\Models\Department;
use Illuminate\Database\Eloquent\Collection;

class DepartmentService implements DepartmentServiceContract
{
    /**
     * {@inheritDoc}
     */
    public function get(): Collection
    {
        return Department::orderBy('name')->get();
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Department
    {
        $department = Department::create($data);

        return $department;
    }

    /**
     * {@inheritDoc}
     */
    public function update(Department $department, array $data): Department
    {
        $department->update($data);

        return $department;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(Department $department): void
    {
        $department->delete();
    }
}
