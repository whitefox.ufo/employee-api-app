<?php

namespace App\Providers;

use App\Contracts\Services\DepartmentServiceContract;
use App\Contracts\Services\EmployeServiceContract;
use App\Services\DepartmentService;
use App\Services\EmployeService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(DepartmentServiceContract::class, DepartmentService::class);
        $this->app->bind(EmployeServiceContract::class, EmployeService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
