<?php

namespace Database\Seeders;

use App\Models\Employe;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $departmentIDs = DB::table('departments')->pluck('id');

        Employe::updateOrCreate(['fullName' => 'One Lore', 'pay' => 100, 'department_id' => $departmentIDs[0]]);
        Employe::updateOrCreate(['fullName' => 'Two Lore', 'pay' => 100, 'department_id' => $departmentIDs[1]]);
        Employe::updateOrCreate(['fullName' => 'Three Lore', 'pay' => 100, 'department_id' => $departmentIDs[2]]);
        Employe::updateOrCreate(['fullName' => 'Four Lore', 'pay' => 100, 'department_id' => $departmentIDs[3]]);
    }
}
