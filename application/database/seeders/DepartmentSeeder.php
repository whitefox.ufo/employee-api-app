<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Department::updateOrCreate(['name' => 'Interns']);
        Department::updateOrCreate(['name' => 'IT']);
        Department::updateOrCreate(['name' => 'Sales']);
        Department::updateOrCreate(['name' => 'Works']);
    }
}
